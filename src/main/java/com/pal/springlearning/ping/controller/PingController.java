package com.pal.springlearning.ping.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/ping")
public class PingController {

    @RequestMapping
    public String pingWithView(
            @RequestParam(name = "content", required = false, defaultValue = "Ping Content") String content,
            Model model) {
        model.addAttribute("content", content);
        return "ping";
    }

}
