package com.pal.springlearning.login.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @RequestMapping("/goToLoginPage")
    public String goToLoginPage() {
        return "login";
    }

}
