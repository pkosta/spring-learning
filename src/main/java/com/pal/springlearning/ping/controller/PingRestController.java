package com.pal.springlearning.ping.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/ping")
public class PingRestController {

    @GetMapping()
    public String getHealthCheck() {
        return "Health check: Server is up and running";
    }

}
